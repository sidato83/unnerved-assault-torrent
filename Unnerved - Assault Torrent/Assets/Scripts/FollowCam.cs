using UnityEngine;

public class FollowCam : MonoBehaviour
{
    // Stores the player's transform
    [SerializeField, Tooltip("Holds the passed in transform")]
    private Transform player;

    // Stores offset values for x, y, and z for cam adjustments
    [SerializeField, Tooltip("For making cam adjustments")]
    private Vector3 offset;

    // Stores a speed value to smooth camera movement transitions
    [SerializeField, Tooltip("Adjust cam movement speed")]
    private float smoothSpeed = 0.125f;

    // Update is called once per frame
    void FixedUpdate()
    {
        // Stores the desired position, set equal to the player's position minus the offset values
        Vector3 desiredPos = player.position + offset;

        // Stores a Vector3 set equal to the linear interpolation between the cam's current position, the desired position, and the speed to move over time
        Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.deltaTime);

        // Sets the cam's transform position to the calculated smoothed position
        transform.position = smoothedPos;
    }
}
