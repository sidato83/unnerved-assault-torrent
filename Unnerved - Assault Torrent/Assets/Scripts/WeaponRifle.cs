using UnityEngine;
using System.Collections;

// Rifle subclass inherits from Weapon
public class WeaponRifle : Weapon
{
    // Stores rifle values
    [Header("Rifle Values")]
    [Tooltip("Rifle bullet capacity")]
    public int magCapacity = 30;

    // Holds prefab for the bullet and transform for the firing point
    [Header("Rifle Components")]
    [SerializeField, Tooltip("Define the bullet prefab")]
    private GameObject rifleBullet;
    [SerializeField, Tooltip("Point to instantiate the bullet")]
    private Transform rifleFirePosition;

    // Allows designer to define rifle weapon burst frequency
    [Header("Burst Fire Settings")]
    [SerializeField, Tooltip("Number of shots in a burst")]
    private int shotsPerBurst = 3;
    [SerializeField, Tooltip("Set delay between burst shots")]
    [Range(0, 1)] private float burstDelay = 0.2f;

    // Override for the base Weapon AttackStart (polymorphism)
    public override void AttackStart()
    {
        // Coroutine to adjust how the rifle fires
        StartCoroutine(FireRifle());
    }

    // Override of the base class AttackEnd (no defined behavior at this time)
    public override void AttackEnd()
    {
        // Calls the base AttackEnd function
        base.AttackEnd();
    }

    // IEnumerator function to fire the rifle with a delay
    IEnumerator FireRifle()
    {
        // Checks if the rifle has bullets
        if (magCapacity > 0)
        {
            // Checks if the the bullet prefab exists and the mouse key has been pressed
            if (rifleBullet != null && Input.GetKeyDown(KeyCode.Mouse0))
            {
                // For loop to iterate through the following lines a designer number of times 
                // Designer value will determine how many shots are fired in a burst
                for (int i = 0; i < shotsPerBurst; i++)
                {
                    // Creates an instantiated bullet prefab at the fire position and rotation
                    GameObject rifleBulletClone = Instantiate(rifleBullet, rifleFirePosition.position, rifleFirePosition.rotation);
                    // Reduces the number of bullets by one with each iteration
                    magCapacity--;

                    // Coroutine allows for a delay between shots (so bullets aren't stacked)
                    yield return new WaitForSeconds(burstDelay);
                }
                
            }
        }
        // Runs if the rifle is out of bullets
        else if (magCapacity <= 0)
        {
            // Displays a reload warning (currently just in the console)
            Debug.Log("RELOAD RIFLE");
        }
    }
}
