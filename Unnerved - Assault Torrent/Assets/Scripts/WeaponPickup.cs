using UnityEngine;

// WeaponPickup subclass inherits from Pickup
public class WeaponPickup : Pickup
{
    // Stores a Weapon instance
    [Header("Weapon List")]
    [SerializeField, Tooltip("Equip a weapon")]
    private Weapon weapon;

    // Protected function that overrides the base OnPickup function (polymorphism)
    protected override void OnPickup(Player player)
    {
        // Stores an Animator instance of the player's Animator 
        Animator anim = player.GetComponent<Animator>();

        // Sets the player weapon to the weapon picked up
        player.weapon = weapon;

        // Checks if the weapon name is "Reaper" (rifle)
        if (weapon.name == "Reaper")
        {
            // Calls the RiflePickedUp function and passes in the player instance
            RiflePickedUp(player);
        }
        else // Runs if the handgun is picked up
        {
            // Calls the HandgunPickedUp function and passes in the player instance
            HandgunPickedUp(player);
        }

        // Sets the "HasWeapon" animator parameter to true
        anim.SetBool("HasWeapon", true);

        // Calls the base OnPickup function (sets the pickup to inactive)
        base.OnPickup(player);
    }

    // Function to equip a rifle
    public void RiflePickedUp(Player player)
    {
        // Sets the player's rifle prefab to active
        player.riflePrefab.SetActive(true);
        // Sets the player's handgun prefab to inactive
        player.handgunPrefab.SetActive(false);
    }

    // Function to equip a handgun
    public void HandgunPickedUp(Player player)
    {
        // Sets the player's handgun prefab to active
        player.handgunPrefab.SetActive(true);
        // Sets the player's rifle prefab to inactive
        player.riflePrefab.SetActive(false);
    }
}
