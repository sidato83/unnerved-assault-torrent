using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // To make the GameManager accessible to the rest of my code
    public static GameManager Instance { get; private set; }

    // Holds a value for the game score
    [Header("Score")]
    [SerializeField, Tooltip("Display a score value")]
    private int gameScore = 0;

    // Holds my player values
    [Header("Player")]
    [Tooltip("Store the player prefab")]
    public Player playerPrefab;
    [SerializeField, Tooltip("Store the player's starting point")]
    private Transform playerSpawnPoint;

    // Stores the amount of player lives
    [Header("Lives")]
    [Tooltip("Set the max player lives")]
    [Range(0,5)] public int maxPlayerLives = 5;

    // Holds the current count for the player's lives
    [HideInInspector]
    public int currentPlayerLives;


    // Runs before Start
    private void Awake()
    {
        // Sets the GameManager Instance to this GameManager
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Sets the value of the player's current life count to the designer defined max life count
        currentPlayerLives = maxPlayerLives;
    }

    // Function used to pause all game elements
    public void OnApplicationPause(bool isPaused)
    {
        // Checks if the passed in boolean is true
        if (isPaused)
        {
            // Sets the timeScale to 0 on all game objects
            Time.timeScale = 0f;
        }
        else // Runs if the passed in value is false
        {
            // Sets the timeScale to the normal scale of 1
            Time.timeScale = 1f;
        }
    }

    // Function used to reduce the player's life count on death
    public void RemoveLife()
    {
        // Reduces the player's current life count by one
        currentPlayerLives--;

        // Checks if the player's life count is less than 1
        if (currentPlayerLives < 1)
        {
            // Calls the GameOver function
            GameOver();
        }
    }

    // Function used to end the game when all player lives are lost
    public void GameOver()
    {
        // The SceneManger loads the "GameOver" scene
        SceneManager.LoadScene("GameOver");
    }
}
