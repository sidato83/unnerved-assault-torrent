using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Stores values to control bullet parameters
    [Header("Bullet Properties")]
    [Tooltip("Amount of force to propel bullet")]
    public float bulletForce = 50f;
    [SerializeField, Tooltip("Bullet time to live")]
    private float bulletLife = 1f;

    [SerializeField, Tooltip("Bullet effect on impact")]
    private GameObject bulletHitParticle;

    // Can be used to assign point values to the shooter
    [HideInInspector]
    public GameObject bulletOwner;

    // Start is called before the first frame update
    void Start()
    {
        // Checks if the bullet still exists
        if (gameObject != null)
        {
            // Destroys the bullet prefab after a designer set time
            Destroy(gameObject, bulletLife);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Used to move the bullet forward with Transform values
        transform.position += transform.forward * bulletForce * Time.deltaTime;
    }

    // Actions to take on gameObject collision
    private void OnCollisionEnter(Collision collision)
    {
        // Creates an instance of a GameObject and sets it equal to a bullet hit particle clone
        GameObject bulletHit = Instantiate(bulletHitParticle, transform.position, transform.rotation);
        // Destroys the particle effect
        Destroy(bulletHit, 1);
        // Destroys the bullet
        Destroy(gameObject);
    }
}
