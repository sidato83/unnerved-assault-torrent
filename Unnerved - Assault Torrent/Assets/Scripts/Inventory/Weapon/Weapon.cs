using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    // Stores the transforms for the inverse kinematic values
    [Header("IK Points")]
    [Tooltip("Weapon right hand placement")]
    public Transform playerRight;
    [Tooltip("Weapon left hand placement")]
    public Transform playerLeft;
    [Tooltip("Weapon right elbow hint")]
    public Transform rightElbow;

    // Holds the enemy's right and left hand positions
    public Transform enemyRight;
    public Transform enemyLeft;
}
