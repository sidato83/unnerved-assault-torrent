using UnityEngine;

public class BulletMagazine : MonoBehaviour
{
    // Stores a magazine capacity integer
    [Header("Magazine Capacity")]
    [SerializeField, Tooltip("Set the max amount of bullets in the magazine")]
    private int magCapacity = 0;

    // Getter and private setter for the current bullet count
    public int bulletCount { get; private set; }

    // Start is called before the first frame update
    private void Start()
    {
        // Sets the current bullet count to the magCapacity amount
        bulletCount = magCapacity;
    }

    // Function to Reload a weapon with the passed in bullet count
    public void ReloadWeapon(int count)
    {
        // The current bullet count is increased by the passed in value
        bulletCount += count;

        // Checks if the current bullet count is greater than the magCapacity
        if (bulletCount > magCapacity)
        {
            // Sets the current bullet count to the magCapacity
            bulletCount = magCapacity;
        }
    }

    // Function to reduce the current bullet count
    public void EjectBulletShell()
    {
        // Checks if the current bullet count is greater than 0
        if (bulletCount > 0)
        {
            // Decreases the bullet count by 1
            bulletCount--;
        }
    }
}
