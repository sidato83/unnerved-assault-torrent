using UnityEngine;

// Handgun subclass inherits from Weapon
public class WeaponHandgun : Weapon
{
    // Bullet prefab and fire position
    [Header("Handgun Components")]
    [SerializeField, Tooltip("Define the bullet prefab")]
    private GameObject bulletPrefab;
    [SerializeField, Tooltip("Point to instantiate the bullet")]
    private Transform bulletFirePosition;

    // Sets the bullet capacity for the Handgun
    private BulletMagazine magazine;

    private void Start()
    {
        // Stores the BulletMagazine component
        magazine = gameObject.GetComponent<BulletMagazine>();
    }

    // Override for the base AttackStart
    public void AttackStart()
    {
        // Calls the function to fire the gun
        FireGun();
    }

    // Function to fire the handgun
    private void FireGun()
    {
        // Checks if the gun has bullets
        if (magazine.bulletCount > 0)
        {
            // Checks if the bullet prefab exists
            if (bulletPrefab != null)
            {
                // Instantiates the bulletPrefab at the fire postion and rotation
                GameObject gunBullet = Instantiate(bulletPrefab, bulletFirePosition.position, bulletFirePosition.rotation);
                FindObjectOfType<AudioManager>().Play("HandgunLaser");
                magazine.EjectBulletShell();
            }
        }
        // Runs if the gun is out of bullets
        else if (magazine.bulletCount <= 0)
        {
            // Display message to reload (just in the console for now)
            Debug.Log("RELOAD GUN");
        }
    }
}