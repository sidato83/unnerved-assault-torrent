using UnityEngine;

public class Inventory : MonoBehaviour
{
    // Weapon inventory for pawns
    [Header("Weapons")]
    public GameObject[] weapons;
}
