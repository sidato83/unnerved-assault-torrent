using UnityEngine;

[RequireComponent(typeof(Health))]
public class Player : MonoBehaviour
{
    // Store weaon values
    [Header("Weapons")]
    [Tooltip("Equip a weapon")]
    // Holds an instance of the Weapon class
    public Weapon weapon;

    // Stores the GameObjects for my projectile weapons
    public GameObject riflePrefab;
    public GameObject handgunPrefab;

    // Holds movement values
    [Header("Player Movement")]
    [SerializeField, Tooltip("The max speed of the player")]
    private float playerMovementSpeed = 4f;

    // Animation action triggers
    // Arrays allow designers to easily adjust actions with KeyCodes
    [Header("Action Triggers")]
    [SerializeField, Tooltip("Map actions to key codes")]
    private string[] playerActions;
    [SerializeField, Tooltip("Assign key to trigger")]
    private KeyCode[] mappedKey;
    [SerializeField, Tooltip("Map toggle actions to key codes")]
    private string[] toggleActions;
    [SerializeField, Tooltip("Assign key to toggle trigger")]
    private KeyCode[] toggleKey;

    // Define the tracking plane for following the mouse
    [Header("Mouse Pointer Tracking")]
    [SerializeField, Tooltip("Used to track the mouse pointer")]
    private LayerMask mouseTrackingPlane;

    // Create animator instance
    private Animator animator;
    // Create a Health instance with getter and private setter
    public Health Health { get; private set; }

    // Start is called before the first frame update
    void Awake()
    {
        // Gets the Animator that this component is attached to
        animator = GetComponent<Animator>();
        Health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        // Called to aim the player at the mouse
        AimTowardMouse();

        // Called to drive player movement
        PlayerMovement();

        // Called to check if the player is grounded
        CheckMiscPlayerValues();

        // Called to activate player triggers with passed in action and mapped key
        AnimationActionTriggers(playerActions, mappedKey);

        // Called to toggle triggers with passed in action and mapped key
        AnimationActionTriggerToggle(toggleActions, toggleKey);
    }
    
    // Function to aim the player at the mouse pointer
    void AimTowardMouse()
    {
        // Creates a ray instance called ray and sets it equal to a ray going from the main camera to the Vector3 position of the mouse pointer
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Checks if the ray, that is cast from the main camera at an infinite length, returns hit information from the defined LayerMask
        if (Physics.Raycast(ray, out var hitInfo, Mathf.Infinity, mouseTrackingPlane))
        {
            // Sets the direction variable equal to point of the ray hitInfo minus the player's transform position
            var direction = hitInfo.point - transform.position;
            // Locks the direction's 'y' value at 0
            direction.y = 0f;
            // Normalizes the direction to a value of 1
            direction.Normalize();
            // Set the player's forward transform equal to the calculated direction
            transform.forward = direction;
        }
    }

    // Function to drive player movement
    void PlayerMovement()
    {
        // Stores the value from the "Horizontal" input
        float horizontal = Input.GetAxis("Horizontal");
        // Stores the value from the "Vertical" input
        float vertical = Input.GetAxis("Vertical");

        // Stores a new Vector3 equal to the value of the horizontal input on the x axis, 0 on the y, and the vertical input on the z)
        Vector3 input = new Vector3(horizontal, 0f, vertical);

        // Stores the value of input with its magnitude clampped to 1
        input = Vector3.ClampMagnitude(input, 1f);
        // Multiplies the input with the player's speed
        input *= playerMovementSpeed;

        // Converts a direction in world space to local space
        // input = transform.InverseTransformDirection(input);

        // Send float values to the animator to define transitions
        animator.SetFloat("Horizontal", input.x, 0.1f, Time.deltaTime);
        animator.SetFloat("Vertical", input.z, 0.1f, Time.deltaTime);
    }

    // Function to set animation trigger boolean value
    void AnimationActionTriggers(string[] action, KeyCode[] mappedKey)
    {
        // Stores the boolean value evaluated in the ternary statement
        // If Mouse0 is held, "isShooting" is true, else false
        bool triggerBoolValue = Input.GetKeyDown(KeyCode.Mouse0) ? true : false;

        // Sets the boolean value for the animator parameter "isShooting"
        animator.SetBool("Shooting", triggerBoolValue);

        // For loop is utilized to iterate through animator parameter string array and KeyCode array
        for (int i = 0; i < playerActions.Length; i++)
        {
            // Stores the boolean value evaluated in the ternary statement
            // If the passed in key is pressed, the passed in action is true, else false
            bool trueFalse = Input.GetKeyDown(mappedKey[i]) ? true : false;

            // Sets the boolean value for the passed in animator parameter
            animator.SetBool(action[i], trueFalse);
        }
    }

    // Function to set animation triggers that are toggled
    void AnimationActionTriggerToggle(string[] action, KeyCode[] mappedKey)
    {
        // For loop to iterate through toggled actions and keys
        for (int i = 0; i < playerActions.Length; i++)
        {
            // Checks if a mapped key has been pressed
            if (Input.GetKeyDown(mappedKey[i]))
            {
                // Checks if boolean on the corresponding action is set to false
                if (animator.GetBool(action[i]) == false)
                {
                    // Sets the boolean on the action to true
                    animator.SetBool(action[i], true);
                }
                // Runs if the action trigger is set to true
                else if (animator.GetBool(action[i]) == true)
                {
                    // Sets the boolean on the action to false
                    animator.SetBool(action[i], false);
                }
            }
        }
    }

    // Function to check miscellaneous player values
    void CheckMiscPlayerValues()
    {
        // Stores the player's CapsuleCollider values
        CapsuleCollider collider = gameObject.GetComponent<CapsuleCollider>();

        // TODO: Add in jump function

        // Checks if boolean for the prone trigger is true
        if (animator.GetBool("Prone") == true)
        {
            // Sets the height of the CapsuleCollider to 0.6
            collider.height = 0.6f;
            // Sets the center of the collider to 0.25 on the y
            collider.center = new Vector3(0f, 0.25f, 0f);
        }
        // Runs if the prone boolean is false
        else
        {
            // Sets the CapsuleCollider values to default levels
            collider.height = 1.85f;
            collider.center = new Vector3(0f, 0.91f, 0f);
        }
    }

    // Function to govern inverse kinematics
    public void OnAnimatorIK(int layerIndex)
    {
        // Check if the player is holding a weapon
        if (weapon != null)
        {
            // Check if the weapon has a defined right and left hand position
            if (weapon.rightHandPosition && weapon.leftHandPosition)
            {
                // Set the IK parameters for the left and right hand positions and rotations
                animator.SetIKPosition(AvatarIKGoal.LeftHand, weapon.leftHandPosition.position);
                animator.SetIKPosition(AvatarIKGoal.RightHand, weapon.rightHandPosition.position);
                animator.SetIKRotation(AvatarIKGoal.LeftHand, weapon.leftHandPosition.rotation);
                animator.SetIKRotation(AvatarIKGoal.RightHand, weapon.rightHandPosition.rotation);

                // IK weight values adjust how firm the sprites hands will attach to an object
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
            }
            else // Runs if two hand positions are not found
            {
                // Sets the IK parameters for single hand weapons
                animator.SetIKPosition(AvatarIKGoal.RightHand, weapon.rightHandPosition.position);
                animator.SetIKRotation(AvatarIKGoal.RightHand, weapon.rightHandPosition.rotation);

                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
            }
        }
    }
}


