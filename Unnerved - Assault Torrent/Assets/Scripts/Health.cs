using UnityEngine.Events;
using UnityEngine;

public class Health : MonoBehaviour
{
    // Defined health values for the attached object
    [Header("Pawn Values")]
    [SerializeField, Tooltip("Pawn max health")]
    private float maxHealth = 100f;
    [SerializeField, Tooltip("Pawn current health")]
    private float currentHealth = 0f;
    [SerializeField, Tooltip("Time to respawn after death")]
    private float respawnTime = 5f;

    // Holds a percentage of health between max and current
    [HideInInspector]
    public float healthPercentage = 0f;

    // Start is called before the first frame update
    private void Awake()
    {
        // Sets the current health to the maxHealth value
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        // Calls the GetHealth function
        GetHealth();
        // Calls the HealthPercet function
        HealthPercent();
    }

    // GetHealth function returns the currentHealth
    float GetHealth() => currentHealth;

    // HealthPercent function returns the value of the current health divided by the max health
    float HealthPercent() => healthPercentage = (currentHealth / maxHealth);

    // Function to add healing to a pawn
    public void TakeHealing(int healing)
    {
        // Adds the passed in heal amount to the current health
        currentHealth += healing;
    }

    // Function to add damage to the pawn
    public void TakeDamage(int damage)
    {
        // Checks if the current health is greater than 0
        if (currentHealth > 0)
        {
            // Subtracts the passed in damage from the current health
            currentHealth -= damage;
        }
        // Runs if the current health is less than or equal to 0
        else
        {
            // Calls the DeadPawn function
            DeadPawn();
        }
    }

    // Function to define what happens to a dead pawn
    public void DeadPawn()
    {
        // Sets the gameObject to inactive
        gameObject.SetActive(false);
        // Sets the current health back to max health
        currentHealth = maxHealth;

        // Invokes the RespawnPawn function after a designer set amount of time
        Invoke("RespawnPawn", respawnTime);
    }

    // Function to respawn a pawn
    private void RespawnPawn()
    {
        // Sets the gameObject back to active
        gameObject.SetActive(true);
    }
}
