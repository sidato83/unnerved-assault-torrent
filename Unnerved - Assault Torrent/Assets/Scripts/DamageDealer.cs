using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    // Store the damage value for whatever object this script is added to
    [Header("Damage Dealer")]
    [SerializeField, Tooltip("Amount of damage done")]
    private int damageDealt = 1;

    // Function to deal damage on collision
    public void OnCollisionEnter(Collision other)
    {
        // Store data from the other object in the collisioin
        GameObject otherObject = other.gameObject;

        // Grab the Health component of the other object
        Health otherHealth = otherObject.GetComponent<Health>();

        // Check if the the other object has Health
        if (otherHealth != null)
        {
            // Call the TakeDamage function from the Health class and pass in the damage dealt
            otherHealth.TakeDamage(damageDealt);

            if (other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
            {
                FindObjectOfType<AudioManager>().Play("PawnHit");
            }
        }
    }
}
