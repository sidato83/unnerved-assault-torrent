using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    // Stores the respawn data for the pickup
    [Header("Pickup Respawn Time")]
    [SerializeField, Tooltip("Time until pickup respawns")]
    private float pickupRespawnTime = 5f;

    // Function that runs after a trigger collision
    private void OnTriggerEnter(Collider collider)
    {
        // Stores the collider of the player
        Player player = collider.GetComponent<Player>();

        // Checks for the player collider
        if (player)
        {
            // Calls the OnPickup function
            OnPickup(player);
        }
    }

    // Function to define what happens when a player triggers a pickup
    protected virtual void OnPickup(Player player)
    {
        // Sets the pickup object to inactive
        gameObject.SetActive(false);
        // Invokes the RespawnPickup function after a designer set amount of time
        Invoke("RespawnPickup", pickupRespawnTime);
    }

    // Function to respawn a pickup
    void RespawnPickup()
    {
        // Sets the pickup object back to active
        gameObject.SetActive(true);
    }
}
