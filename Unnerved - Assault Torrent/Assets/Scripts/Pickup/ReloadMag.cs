using UnityEngine;

public class ReloadMag : MonoBehaviour
{
    // Sets an ammo count for any gun weapon
    [Header("Mag Capacity")]
    [Tooltip("Max reload ammo amount")]
    public int ammoCount;
}
