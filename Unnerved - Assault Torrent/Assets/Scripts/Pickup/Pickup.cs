using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    // Stores the respawn data for the pickup
    [Header("Pickup Respawn Time")]
    [SerializeField, Tooltip("Time until pickup respawns")]
    private float pickupRespawnTime = 5f;

    // Function that runs after a trigger collision
    private void OnTriggerEnter(Collider other)
    {
        // Stores the pawn types of the object that enters the trigger
        Player player = other.gameObject.GetComponent<Player>();
        Enemy enemy = other.gameObject.GetComponent<Enemy>();

        // Checks for the player type
        if (player)
        {
            // Calls the OnPickup function with the passed in player
            OnPlayerPickup(player);
        }
        else // Runs if the pawn type is not a player
        {
            // Calls the OnEnemyPickup function with the passed in enemy
            OnEnemyPickup(enemy);
        }
    }

    // Function to define what happens when a player triggers a pickup
    protected virtual void OnPlayerPickup(Player player)
    {
        // Sets the pickup object to inactive
        gameObject.SetActive(false);
        // Invokes the RespawnPickup function after a designer set amount of time
        Invoke("RespawnPickup", pickupRespawnTime);
    }

    // Function to define what happens when a player triggers a pickup
    protected virtual void OnEnemyPickup(Enemy enemy)
    {
        // Sets the pickup object to inactive
        gameObject.SetActive(false);
        // Invokes the RespawnPickup function after a designer set amount of time
        Invoke("RespawnPickup", pickupRespawnTime);
    }

    // Function to respawn a pickup
    void RespawnPickup()
    {
        // Sets the pickup object back to active
        gameObject.SetActive(true);
    }
}
