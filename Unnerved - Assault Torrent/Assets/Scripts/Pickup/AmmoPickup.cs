using UnityEngine;
using UnityEngine.Events;

public class AmmoPickup : Pickup
{
    // Protected override of the base OnPickup function (polymorphism)
    protected override void OnPlayerPickup(Player player)
    {
        // Calls the ReloadWeapon method from the Player class
        player.ReloadWeapon();

        // Calls the base OnPickup function (deactivates pickup)
        base.OnPlayerPickup(player);
    }

    // Protected override of the base OnPickup function (polymorphism)
    protected override void OnEnemyPickup(Enemy enemy)
    {
        // No reloading for enemies
        return;
    }
}
