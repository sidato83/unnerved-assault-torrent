using UnityEngine;
using Random = System.Random;
using System.Linq;
using System;

[System.Serializable]
public class WeightedItemDrops
{
    // Store weighted item objects to drop
    [SerializeField, Tooltip("The item at this index")]
    private GameObject item;
    // Store a drop chance value
    [SerializeField, Tooltip("The chance to roll this item")]
    private double chance = 1;

    // Stores a random value
    private static Random rnd;

    // Holds an array of doubles
    private static double[] cdfArray;

    // Function selects a random value from an array and returns the GameObject at that value
    public static GameObject SelectItem(WeightedItemDrops[] items)
    {
        // Sets a new random
        rnd = new Random();

        // Builds the CDF Array from the passed in items
        cdfArray = PopulateCdfArray(items);

        // Sets an index value equal to the BinarySearch value of the cdfArray
        // Between a random double and the last value of the cdfArray
        int selectedIndex = Array.BinarySearch(cdfArray, rnd.NextDouble() * cdfArray.Last());

        // Checks if the selected index is less than 0
        if (selectedIndex < 0)
        {
            // Bitwise shift gives an idea of where the value falls if an exact value isn't found
            selectedIndex = ~selectedIndex;
        }
        
        // Returns the weighted GameObject at the selected index
        return items[selectedIndex].item;
    }

    // Function builds a CDF Array of doubles and returns the array
    static double[] PopulateCdfArray(WeightedItemDrops[] items)
    {
        // Sets the cdfArray to a new double array sized by the number of passed in items
        cdfArray = new double[items.Length];
        // Holds a chance total value
        double chanceTotal = 0;

        // For loop will iterate through each of the passed in items
        for (int i = 0; i < items.Length; i++)
        {
            // Sets the value of the current index equal to the chance value of the passed in item plus the current chanceTotal
            cdfArray[i] = items[i].chance + chanceTotal;

            // Increments the chanceTotal by the amount of the current chance
            chanceTotal += items[i].chance;
        }

        // Returns the generated array
        return cdfArray;
    }
}
