using UnityEngine;

// WeaponPickup subclass inherits from Pickup
public class WeaponPickup : Pickup
{
    // Stores a Weapon instance
    [Header("Weapon List")]
    [SerializeField, Tooltip("Set the player weapon")]
    private Weapon playerWeapon;
    [SerializeField, Tooltip("Set the enemy weapon")]
    private Weapon enemyWeapon;

    // Protected function that overrides the base OnPickup function (polymorphism)
    protected override void OnPlayerPickup(Player player)
    {
        // Stores an Animator instance of the player's Animator 
        Animator anim = player.GetComponent<Animator>();

        // Checks if the weapon name is "Reaper" (rifle)
        if (playerWeapon.tag == "Rifle")
        {
            // Calls the RiflePickedUp function and passes in the player instance
            PlayerRiflePickedUp(player);
        }
        else // Runs if the handgun is picked up
        {
            // Calls the HandgunPickedUp function and passes in the player instance
            PlayerHandgunPickedUp(player);
        }

        // Sets the "HasWeapon" animator parameter to true
        anim.SetBool("HasWeapon", true);

        // Calls the base OnPickup function (sets the pickup to inactive)
        base.OnPlayerPickup(player);
    }

    // Protected function that overrides the base OnPickup function (polymorphism)
    protected override void OnEnemyPickup(Enemy enemy)
    {
        // Checks if the weapon name is "Reaper" (rifle)
        if (enemyWeapon.tag == "Rifle")
        {
            // Calls the RiflePickedUp function and passes in the player instance
            EnemyRiflePickedUp(enemy);
        }
        else // Runs if the handgun is picked up
        {
            // Calls the HandgunPickedUp function and passes in the player instance
            EnemyHandgunPickedUp(enemy);
        }

        // Calls the base OnPickup function (sets the pickup to inactive)
        base.OnEnemyPickup(enemy);
    }

    // Function to equip a rifle
    public void PlayerRiflePickedUp(Player player)
    {
        // Stores the inventory (weapons) of the player pawn
        Inventory inventory = player.GetComponent<Inventory>();

        // Sets the player's rifle prefab to active
        inventory.weapons[0].SetActive(true);
        // Sets the player's handgun prefab to inactive
        inventory.weapons[1].SetActive(false);

        // Sets the passed in player's weapon to the picked up weapon
        player.playerWeapon = inventory.weapons[0].GetComponent<Weapon>();
    }

    // Function to equip a handgun
    public void PlayerHandgunPickedUp(Player player)
    {
        // Stores the player's inventory (weapons)
        Inventory inventory = player.GetComponent<Inventory>();

        // Sets the player's handgun prefab to active
        inventory.weapons[1].SetActive(true);
        // Sets the player's rifle prefab to inactive
        inventory.weapons[0].SetActive(false);

        // Sets the player's weapon to the picked up weapon
        player.playerWeapon = inventory.weapons[1].GetComponent<Weapon>();
    }

    // Function to equip a rifle
    public void EnemyRiflePickedUp(Enemy enemy)
    {
        if (enemy != null)
        {
            // Stores the enemy's inventory (weapons)
            Inventory inventory = enemy.GetComponent<Inventory>();

            // Sets the player's rifle prefab to active
            inventory.weapons[0].SetActive(true);
            // Sets the player's handgun prefab to inactive
            inventory.weapons[1].SetActive(false);

            // Sets the enemy weapon to the weapon picked up
            enemy.enemyWeapon = inventory.weapons[0].GetComponent<Weapon>();
        }
    }

    // Function to equip a handgun
    public void EnemyHandgunPickedUp(Enemy enemy)
    {
        if (enemy != null)
        {
            // Stores the enemy's inventory (weapons)
            Inventory inventory = enemy.GetComponent<Inventory>();

            // Sets the enemy's handgun prefab to active
            inventory.weapons[1].SetActive(true);
            // Sets the enemy's rifle prefab to inactive
            inventory.weapons[0].SetActive(false);

            // Sets the enemy weapon to the weapon picked up
            enemy.enemyWeapon = inventory.weapons[1].GetComponent<Weapon>();
        }
    }
}
