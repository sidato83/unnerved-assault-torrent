using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Stores values to control bullet parameters
    [Header("Bullet Properties")]
    [SerializeField, Tooltip("Amount of force to propel bullet")]
    private float bulletForce = 50f;
    [SerializeField, Tooltip("Bullet time to live")]
    private float bulletLife = 1f;

    // Can be used to assign point values to the shooter
    [SerializeField, Tooltip("To track who shot the bullet")]
    private GameObject bulletOwner;

    // Start is called before the first frame update
    void Start()
    {
        // Checks if the bullet still exists
        if (gameObject != null)
        {
            // Destroys the bullet prefab after a designer set time
            Destroy(gameObject, bulletLife);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Used to move the bullet forward with Transform values
        transform.position += transform.forward * bulletForce * Time.deltaTime;
    }
}
