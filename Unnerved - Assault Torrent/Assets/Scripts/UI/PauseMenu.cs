using UnityEngine.UI;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    // Read the Tooltip
    [Header("UI Elements to Toggle")]
    [SerializeField, Tooltip("Store image elements that will toggle with the pause menu")]
    private Image[] uiElements;
    [Header("Weapon Display Image")]
    [SerializeField, Tooltip("Store the image for the weapon display")]
    private WeaponDisplay weaponDisplay;

    // Holds the Animator for the pause menu
    [Header("Pause Menu Animator")]
    [SerializeField]
    private Animator menuOpenClose;

    // Allows the designer to control how fast the pause menu opens
    [Header("Pause Menu Animation Scale Speed")]
    [Range(0, 1)] public float scaleSpeed = 0.6f;

    // Booleans to toggle UI and game elements
    [HideInInspector]
    public bool isVisible = true;
    [HideInInspector]
    public bool isPaused = false;

    // Runs before Start
    private void Awake()
    {
        // Sets the value for the pause menu Animator
        menuOpenClose = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if input is received on the "P" key
        if (Input.GetKeyDown(KeyCode.P))
        {
            // Called to set the menu scale and passes in the scale speed
            ScaleOpenMenu(scaleSpeed);
            // Called to toggle the persistent UI elements
            ToggleUIElements(isVisible);
        }
    }

    // Function used to scale the size of the pause menu at the speed passed in
    public void ScaleOpenMenu(float scaleSpeed)
    {
        // Toggles the isPaused boolean with every call to the method
        isPaused = !isPaused;

        // Sets the menu Animator speed equal to the passed in value times a hard coded multiplier
        menuOpenClose.speed = scaleSpeed * 5;
        // Sets the Animator boolean to the isPaused value to control open and close animations
        menuOpenClose.SetBool("isPaused", isPaused);
        
        // Calls the onApplicationPause() method from the GameManager and passes in the isPaused value
        GameManager.Instance.OnApplicationPause(isPaused);
    }

    // Function to toggle the persistent UI elements in the scene
    public void ToggleUIElements(bool state)
    {
        // For each loop will iterate through each item in the uiElements array
        foreach (var element in uiElements)
        {
            // Sets the element at each index to inactive
            element.gameObject.SetActive(!state);
        }

        // Checks if the weapon display is showing a weapon
        if (weaponDisplay.weaponEquipped)
        {
            // Sets the weapon display to the oposite of the passed in option
            weaponDisplay.gameObject.SetActive(!state);
        }

        // Sets the UI isVisible boolean to the opposite of the passed in value
        isVisible = !state;
    }
}
