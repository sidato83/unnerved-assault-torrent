using UnityEngine;
using UnityEngine.Audio;

public class MenuSliders : MonoBehaviour
{
    // Store AudioMixer instances
    public AudioMixer audioMixer;

    // Function to adjust the MainVolume (music) mixer
    public void MasterVolume (float volume)
    {
        // Sets the mixer's volume float to the passed in float value
        audioMixer.SetFloat("Master", volume);
    }

    public void MusicVolume (float volume)
    {
        audioMixer.SetFloat("Music", volume);
    }

    // Function to adjust the SFX Volume
    public void SFXVolume(float volume)
    {
        // Sets the mixer's volume float to the passed in float value
        audioMixer.SetFloat("SFX", volume);
    }
}
