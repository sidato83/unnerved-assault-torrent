using UnityEngine;
using UnityEngine.UI;

public class WeaponDisplay : MonoBehaviour
{
    // Stores the UI Image for the display background
    [Header("Weapon Display Background")]
    [SerializeField, Tooltip("Set the background for the weapon display")]
    private Image weaponDisplayBG;

    // Stores the images to display for each equipped weapon
    [Header("Weapons To Equip")]
    [SerializeField, Tooltip("Image array for weapon icons")]
    private Image[] weaponIcons;

    // Stores the moving animation and animation values
    [Header("Weapon Display Flourish")]
    [SerializeField, Tooltip("UI Flourish for displayed weapon")]
    private Image weaponDisplayFlourish;
    [SerializeField, Tooltip("Flourish rotation speed")]
    private float imageRotateSpeed = 20f;

    // Holds a reference to the Player
    private Player player;

    // Boolean to determine if a weapon should be displayed
    [HideInInspector]
    public bool weaponEquipped;

    // Start is called before the first frame update
    void Start()
    {
        // Stores an instance of the Player
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        // Called to animate the passed in weapon display images
        RotateFlourish(weaponDisplayFlourish); 
    }

    // Function used to rotate the passed in image
    void RotateFlourish(Image flourishImage)
    {
        // Rotates the transform of the image on the z axis over time
        flourishImage.transform.Rotate(0f, 0f, -imageRotateSpeed * Time.deltaTime);
    }

    // Function used to toggle the weapon displayed
    public void DisplayWeapon()
    {
        // Checks if the player has a weapon equipped
        if (player.playerWeapon != null)
        {
            // Sets the weapon display features to active
            weaponDisplayBG.gameObject.SetActive(true);
            weaponDisplayFlourish.gameObject.SetActive(true);

            // Checks if the player's equipped weapon is a Rifle
            if (player.playerWeapon.tag == "Rifle")
            {
                // Sets the object at the first index to true and the second index to false
                // TODO: REFACTOR | Hard coded values won't scale
                weaponIcons[0].gameObject.SetActive(true);
                weaponIcons[1].gameObject.SetActive(false);
            }
            // Runs if the player's equipped weapon is a Handgun
            else if (player.playerWeapon.tag == "Handgun")
            {
                // Sets the array indices to their opposite values
                weaponIcons[0].gameObject.SetActive(false);
                weaponIcons[1].gameObject.SetActive(true);
            }

            // Sets the weaponEquipped boolean to true
            weaponEquipped = true;
        }
        else // Runs if the player is not holding a weapon
        {
            // Deactivates the UI animation features
            weaponDisplayFlourish.gameObject.SetActive(false);

            // For loop will iterate through the weapon icons array
            for (int i = 0; i < weaponIcons.Length; i++)
            {
                // Sets the weapon icons to inactive
                weaponIcons[i].gameObject.SetActive(false);
            }
        }
    }
}
