using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// *** THIS CODE WAS OBTAINED FROM JASON WEIMANN ON YOUTUBE: THIS IS NOT MY CODE *** | https://www.youtube.com/watch?v=CA2snUe7ARM

public class HealthBar : MonoBehaviour
{
    // Stores an image for a healthbar foreground
    [SerializeField, Tooltip("Set foreground for health bar")]
    private Image foregroundImage;

    // Implements a healthbar smoothing effect
    [SerializeField, Tooltip("Health bar smoothing time")]
    private float updateSpeedSeconds = 0.5f;

    private void Awake()
    {
        // Grabs the Health component of the parent object listens for OnHealthPctChanged
        // Calls HealthChanged and passes in the health percentage from the action Event
        GetComponentInParent<Health>().OnHealthPctChanged += HealthChanged;
    }

    // Function to display the proper health bar values
    private void HealthChanged(float percent)
    {
        // Coroutine to display the health bar with passed in percentage
        StartCoroutine(DisplayHealth(percent));
    }

    // Function to control the health bar adjustments with smoothing
    private IEnumerator DisplayHealth(float percent)
    {
        // Stores a float that will govern the fill amount for the foreground image on the health bar
        float preChange = foregroundImage.fillAmount;
        // Stores the float to adjust smoothing
        float elapsedTime = 0f;

        // Runs while the elapsed time is less than the update speed
        while (elapsedTime < updateSpeedSeconds)
        {
            // The elapsed time increases with each frame
            elapsedTime += Time.deltaTime;
            // Interpolates between the pre-change health bar fill and the percentage changed over elapsed time divided by update speed
            foregroundImage.fillAmount = Mathf.Lerp(preChange, percent, elapsedTime / updateSpeedSeconds);

            // Null yield return for IEnumerator
            yield return null;
        }

        // Sets the forground image fill amount to the passed in percentage
        foregroundImage.fillAmount = percent;
    }
}
