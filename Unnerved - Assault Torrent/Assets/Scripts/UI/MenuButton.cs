using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public Animator pauseAnim;
    public PauseMenu pause;

   // Function to Start the Game (Load Scene "Main")
   public void StartButton()
    {
        // SceneManager grabs the current scene and loads the scene at the next index
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Function to quit the application
    public void QuitButton()
    {
        // Calls the Quit function from the Application class
        Application.Quit();
    }

    // Function to restart the game
    public void RestartButton()
    {
        // SceneManager is set to grab the "Main" scene
        SceneManager.LoadScene("Main");
    }

    // Function to load the main menu
    public void MainMenu()
    {
        // SceneManager is set to grab the "MainMenu" scene
        SceneManager.LoadScene("StartMenu");
    }

    // Function to Resume the game after pausing
    public void ResumeButton()
    {
        // Stores the current isVisible boolean from the PauseMenu
        bool visible = pause.isVisible;
        // Stores the defined scaleSpeed from the PauseMenu
        float scaleSpeed = pause.scaleSpeed;

        // Calls the ScaleOpenMenu and passes in the scaleSpeed
        pause.ScaleOpenMenu(scaleSpeed);
        // Calls the UI toggle function and passes in the isVisible boolean
        pause.ToggleUIElements(visible);
    }

}
