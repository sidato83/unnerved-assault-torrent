using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class HealthText : MonoBehaviour
{
    // Creates a Health instance called healthPercent
    public Health healthPercent;
    // Creates a TextMeshPro instance called healthText
    private TextMeshProUGUI healthText;

    [Header("Life Count")]
    [SerializeField, Tooltip("Store life icon images")]
    private Image[] lifeIcon;

    // Called before start
    private void Awake()
    {
        // Stores the TextMestPro component in healthText
        healthText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        // Calls the DisplayLives function
        DisplayLives();

        // Checks if healtText is not null
        if (healthText != null)
        {
            // Sets the TMP text as a string, formatting health as a percentage and multiplying by 100 to show whole values
            healthText.text = string.Format("Health: {0}%", Mathf.RoundToInt(healthPercent.healthPercentage * 100f));
        }
    }

    // Function used to display the player's current lives
    void DisplayLives()
    {
        // For loop will iterate through the length of the lifeIcon array
        for (int i = 0; i < lifeIcon.Length; i++)
        {
            // Checks if the iteration count is less than the current number of player lives
            if (i < GameManager.Instance.currentPlayerLives)
            {
                // All life icons are enabled up to the number of player lives
                lifeIcon[i].enabled = true;
            }
            else // Runs if the iteration count is greater than the amount of player lives
            {
                // All extra icons over the player life count remain disabled
                lifeIcon[i].enabled = false;
            }
        }
    }
}
