using System.Collections.Generic;
using UnityEngine;
using TMPro;

// THIS IS NOT MY CODE: SHOUT OUT TO BRACKEYS FOR GETTING ME ACROSS THE FINISH LINE!! https://youtu.be/YOaYQrN1oYQ

public class ScreenResolution : MonoBehaviour
{
    // Will hold the UI dropdown object
    public TMP_Dropdown resolutionDropdown;

    // Creates a resolution array
    Resolution[] resolutions;

    private void Start()
    {
        // Generates the resolution dropdown with local screen values
        SetResolutionDropdown();
        // Preset medium quality
        SetGraphicsQuality(1);
    }

    // Function to set the screen resolution
    public void SetResolution(int resolutionIndex)
    {
        // Stores a resolution instance and sets it equal to the passed in index value in the resolution array
        Resolution resolution = resolutions[resolutionIndex];
        // Sets the screen resolution to the width and height of the passed in index
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    // Function to set graphics quality
    public void SetGraphicsQuality(int qualityIndex)
    {
        // Sets the QualitySettings level to the passed in value
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // Function to toggle full screen mode
    public void SetFullScreen(bool isFullscreen)
    {
        // Sets the fullScreen boolean to the passed in value
        Screen.fullScreen = isFullscreen;
    }

    // Function to display computer resolutions
    void SetResolutionDropdown()
    {
        // Sets the resolutions array equal to all available screen options
        resolutions = Screen.resolutions;

        // Clears the default dropdown options
        resolutionDropdown.ClearOptions();

        // Creates a new string list called options
        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        // For loop will iterate through all resolutions in the array
        for (int i = 0; i < resolutions.Length; i++)
        {
            // Sets the string option equal to the resolution width and height
            string option = resolutions[i].width + "x" + resolutions[i].height;
            // Adds each string option to the list
            options.Add(option);

            // Checks if each resolution in the array is equal to the current screen resolution
            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                // Stores the index of the matching value
                currentResolutionIndex = i;
            }
        }

        // Displays the resolution dropdown with the string width/height values
        resolutionDropdown.AddOptions(options);
        // Sets the dropdown value to the matched index above
        resolutionDropdown.value = currentResolutionIndex;
        // Refreshes the dropdown display to show current resolution
        resolutionDropdown.RefreshShownValue();
    }
}
