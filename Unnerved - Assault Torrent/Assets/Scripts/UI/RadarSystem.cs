using UnityEngine;
using UnityEngine.UI;

public class RadarSystem : MonoBehaviour
{
    [Header("Icon Movement")]
    [SerializeField, Tooltip("Icon to move with the player")]
    private Image playerIcon;
    [SerializeField, Tooltip("Background icon movement")]
    private Image[] backgroundImages;
    [SerializeField, Tooltip("Background move speed")]
    private float[] bgMoveSpeeds;

    public float iconMoveSpeed;

    private Player player;

    private void Awake()
    {
        player = FindObjectOfType<Player>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RotateBackgroundElements();
    }

    void RotateBackgroundElements()
    {
        for (int i = 0; i < backgroundImages.Length; i++)
        {
            backgroundImages[i].transform.Rotate(0f, 0f, bgMoveSpeeds[i] * Time.deltaTime);
        }
    }
}
