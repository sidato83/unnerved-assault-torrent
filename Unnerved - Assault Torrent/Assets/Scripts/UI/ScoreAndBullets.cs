using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ScoreAndBullets : MonoBehaviour
{
    // Holds a BulletMagazine instance
    [Header("Current Player Magazine")]
    [SerializeField, Tooltip("Access the Bullet Magazine for the current bullet count")]
    private BulletMagazine[] magazine;

    // Store the text that will display the bullet count
    [Header("Bullet Count")]
    [SerializeField, Tooltip("Display the current bullet count")]
    private TextMeshProUGUI bulletCount;

    // Set a player instance to get player values
    private Player player;

    // TODO: Add score values for the GUI Display

    // Called before Start
    private void Awake()
    {
        // Set the bullet count TextMeshPro
        bulletCount = GetComponent<TextMeshProUGUI>();
        // Store the player instance
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.playerWeapon != null)
        {
            // Checks if the TextMesh has been defined for the bullet count and if the player is holding a rifle
            if (bulletCount != null && player.playerWeapon.tag == "Rifle")
            {
                // Displays the bullet count text, in comma format, from the magazine's bulletCount
                bulletCount.text = string.Format("{0:n0}", magazine[0].bulletCount);
            }
            else // Runs if the player is holding the handgun
            {
                bulletCount.text = string.Format("{0:n0}", magazine[1].bulletCount);
            }
        }
    }
}
