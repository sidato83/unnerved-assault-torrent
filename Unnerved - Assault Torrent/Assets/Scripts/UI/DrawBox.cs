using UnityEngine;

public class DrawBox : MonoBehaviour
{
    // Store a Vector3 value
    public Vector3 scale;

    // Function to draw gizmos in the game scene
    private void OnDrawGizmos()
    {
        // Defines a 4x4 matrix
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        // Set the color of the gizmo
        Gizmos.color = Color.Lerp(Color.cyan, Color.clear, 0.5f);
        // Draw a cube and define dimensions
        Gizmos.DrawCube(Vector3.up * scale.y / 2f, scale);
        // Set the color
        Gizmos.color = Color.cyan;
        // Draw a ray to indicate gizmo forward
        Gizmos.DrawRay(Vector3.zero, Vector3.forward * 0.4f);
    }
}
