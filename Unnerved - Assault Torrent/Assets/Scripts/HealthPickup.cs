using UnityEngine;

// HealthPickup subclass inherits from Pickup
public class HealthPickup : Pickup
{
    // Holds a heal value
    [Header("Heal Value")]
    [SerializeField, Tooltip("Amount of healing done")]
    private int healValue = 5;

    // Protected override of the base OnPickup function (polymorphism)
    protected override void OnPickup(Player player)
    {
        // Stores a Health instance of the player's health
        Health health = player.GetComponent<Health>();

        // Calls the TakeHealing function from Health
        health.TakeHealing(healValue);

        // Calls the base OnPickup function (deactivates pickup)
        base.OnPickup(player);
    }
}
