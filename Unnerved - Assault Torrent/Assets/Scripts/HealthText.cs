using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class HealthText : MonoBehaviour
{
    // Creates a Health instance called healthPercent
    public Health healthPercent;
    // Creates a TextMeshPro instance called healthText
    private TextMeshProUGUI healthText;

    // Called before start
    private void Awake()
    {
        // Stores the TextMestPro component in healthText
        healthText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (healthText != null)
        {
            // Sets the TMP text as a string, formatting health as a percentage and multiplying by 100 to show whole values
            healthText.text = string.Format("Health: {0}%", Mathf.RoundToInt(healthPercent.healthPercentage * 100f));
        }
    }
}
