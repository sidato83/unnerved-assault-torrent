using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

// Enemy requires NavMeshAgent and Animator componenets
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]

public class Enemy : Pawn
{
    // Store weaon values
    [Header("Weapons")]
    [Tooltip("Equip a weapon")]
    // Holds an instance of the Weapon class
    public Weapon enemyWeapon;

    [Header("Attack Detection")]
    [SerializeField, Tooltip("Distance to attack the player")]
    private float enemyAttackRange = 2f;
    [SerializeField, Tooltip("Frequency of the enemy attack")]
    private float attackRate = 2f;

    private bool playerInAttackRange;
    private bool alreadyAttacked = false;
    private Player player;

    [SerializeField, Tooltip("To detect the ground")]
    private LayerMask groundMask;
    [SerializeField, Tooltip("To detect the player")]
    private LayerMask playerMask;

    // Store enemy navigation values
    private Transform playerTarget;
    private Vector3 desiredVelocity;

    // Store the enemy NavMeshAgent and Animator
    private NavMeshAgent enemyNavMesh;

    // Used for weighted item drops
    [Header("Item Drop Settings")]
    [SerializeField, Tooltip("Set a value for the chance to drop an item")]
    [Range(0, 1)] private float dropChance = 0.5f;
    public WeightedItemDrops[] itemDrops;
    [SerializeField, Tooltip("Set item drop offset value")]
    private Vector3 itemDropOffset;

    // Enemy attack events
    public UnityEvent enemyRifleAttack;
    public UnityEvent enemyHandgunAttack;

    // Awake is called before start
    protected override void Awake()
    {
        // Set the enemyNavMesh to the NavMeshAgent of the attached gameObject
        enemyNavMesh = gameObject.GetComponent<NavMeshAgent>();
        // Set the enemyWeapon to the gameObjects Weapon component
        enemyWeapon = gameObject.GetComponent<Weapon>();

        // Calls to the Pawn class (base Awake function)
        base.Awake();

        // Adds onDeath listener that invokes the ragdoll funtion
        health.onDeath.AddListener(InvokeRagdoll);
    }

    // Start is called before the first frame update
    void Start()
    {
        // Sets the player target for the enemy to navigate to
        playerTarget = FindObjectOfType<Player>().transform;
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        // Calls the movement function for the enemy
        PawnMovement();

        // Check if the player target is in range
        DefineEnemyState();
    }

    // Invoked each frame after animation have been evaluated
    private void OnAnimatorMove()
    {
        // Sets the enemyNavMest velocity to the animator velocity value
        enemyNavMesh.velocity = anim.velocity;
    }

    // Function to define the enemy's movements
    void PawnMovement()
    {
        // Sets the desired velocity of the enemy, adding in the consideration of the enemy's acceleration
        desiredVelocity = Vector3.MoveTowards(desiredVelocity, enemyNavMesh.desiredVelocity, enemyNavMesh.acceleration * Time.deltaTime);

        // Simulates input for the enemy pawn by passing in the desired velocity
        Vector3 input = transform.InverseTransformDirection(desiredVelocity);

        // Send float values to the anim to define transitions
        anim.SetFloat("Horizontal", input.x, 0.1f, Time.deltaTime);
        anim.SetFloat("Vertical", input.z, 0.1f, Time.deltaTime);
    }

    // Function to check how close the player target is
    void DefineEnemyState()
    {
        // Sets the boolean for the playerInAttackRange, based on the range of the enemy checkSpere
        playerInAttackRange = Physics.CheckSphere(transform.position, enemyAttackRange, playerMask);
        WeaponPickup weapon = FindObjectOfType<WeaponPickup>();

        // Checks if the player is not in attack range
        if (health.currentHealth > 0 && enemyWeapon == null)
        {
            if (weapon.transform != null)
            {
                // Calls the ChaseTarget function and passes in a weapon pickup
                ChaseTarget(weapon.transform);
            }
        }
        else if (health.currentHealth > 0)
        {
            // Checks if the player still exists
            if (player != null)
            {
                // Checks if the player is in attack range
                if (playerInAttackRange)
                {
                    // Calls the AttackPlayer function and passes in the Player
                    AttackPlayer(player);
                }
                else
                {
                    // Calls the ChaseTarget function and passes in the desired target
                    ChaseTarget(playerTarget);
                }
            }
        }
    }

    // Function to send the enemy to the target
    void ChaseTarget(Transform target)
    {
        // Checks if the target exists
        if (target != null)
        {
            // Sets the enemyNavMesh destination to the targets position
            enemyNavMesh.SetDestination(target.position);
        }
    }

    // Function to attack the player target
    void AttackPlayer(Player target)
    {
        // Sets the enemyNavMesh destination to the enemy's current transform position
        enemyNavMesh.SetDestination(transform.position);
        // Causes the enemy to look in the direction of the player's transform
        transform.LookAt(target.transform);

        // Checks if the enemy has a weapon equipped and the player is in attack range
        if (enemyWeapon != null && playerInAttackRange)
        {
            // Checks if the alreadyAttacked boolean is false
            if (!alreadyAttacked && gameObject != null)
            {
                // Checks if the enemy weapon is a rifle
                if (enemyWeapon.GetComponent<WeaponRifle>())
                {
                    // Invokes the enemyRifleAttack event
                    enemyRifleAttack.Invoke();
                }
                // Checks if the enemy weapon is a handgun
                else if (enemyWeapon.GetComponent<WeaponHandgun>())
                {
                    // Invokes the enemyHandgunAttack event
                    enemyHandgunAttack.Invoke();
                }
                
                // Sets the alreadyAttacked boolean to true
                alreadyAttacked = true;

                // Invokes the AttackReset function based on the attackRate
                Invoke("AttackReset", attackRate);
            }
        }
    }

    // Function to reset the alreadyAttacked boolean after the designated time
    void AttackReset()
    {
        // Sets the boolean to false
        alreadyAttacked = false;        
    }

    // Function to activate the ragdoll effect
    void InvokeRagdoll()
    {
        if (enemyWeapon != null)
        {
            // Deactivates the enemy weapon on death
            enemyWeapon.gameObject.SetActive(false);
        }

        // Called to drop a random item on enemy death
        RandomItemDrop();

        // Grabs the Enemy Animator and disables it
        GetComponent<Animator>().enabled = false;

        // Calls to the base class function to set the rigidbody state to false
        SetRigidbodyState(false);
        // Calls to the base class function to set all ragdoll colliders to true
        SetColliderState(true);
    }

    // Function to drop a random item from an Enemy
    public void RandomItemDrop()
    {
        // Checks if a randomly generated number, between 0 and 1, is less than the designer defined drop chance
        if (Random.Range(0f, 1f) < dropChance)
        {
            // Instantiates a random weighted item at the enemy's position and rotation
            Instantiate(WeightedItemDrops.SelectItem(itemDrops), transform.position + itemDropOffset, Quaternion.identity);
        }
    }
    
    // Function to govern inverse kinematics
    void OnAnimatorIK(int layerIndex)
    {
        // Check if the player is holding a weapon
        if (enemyWeapon != null)
        {
            // Check if the weapon has a defined right and left hand position
            if (enemyWeapon.enemyRight && enemyWeapon.enemyLeft)
            {
                // Set the IK parameters for the left and right hand positions and rotations
                anim.SetIKPosition(AvatarIKGoal.LeftHand, enemyWeapon.enemyLeft.position);
                anim.SetIKPosition(AvatarIKGoal.RightHand, enemyWeapon.enemyRight.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, enemyWeapon.enemyLeft.rotation);
                anim.SetIKRotation(AvatarIKGoal.RightHand, enemyWeapon.enemyRight.rotation);

                // IK weight values adjust how firm the sprites hands will attach to an object
                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
            }
            else // Runs if two hand positions are not found
            {
                // Sets the IK parameters for single hand weapons
                anim.SetIKPosition(AvatarIKGoal.RightHand, enemyWeapon.enemyRight.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, enemyWeapon.enemyRight.rotation);
                
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
            }
        }
    }

    // Function to visualize the selected Gizmo
    private void OnDrawGizmosSelected()
    {
        // Sets the Gizmo color to red
        Gizmos.color = Color.red;
        // Draws the gizmo, takes in the center and the radius
        Gizmos.DrawWireSphere(transform.position, enemyAttackRange);
    }
}
