using UnityEngine;
using UnityEngine.Events;

public class Player : Pawn
{
    // Store weaon values
    [Header("Weapons")]
    [Tooltip("Equip a weapon")]
    // Holds an instance of the Weapon class
    public Weapon playerWeapon;

    // Holds movement values
    [Header("Player Movement")]
    [SerializeField, Tooltip("The max speed of the player")]
    private float playerMovementSpeed = 4f;

    // Animation action triggers
    // Arrays allow designers to easily adjust actions with KeyCodes
    [Header("Action Triggers")]
    [SerializeField, Tooltip("Map actions to key codes")]
    private string[] playerActions;
    [SerializeField, Tooltip("Assign key to trigger")]
    private KeyCode[] mappedKey;
    [SerializeField, Tooltip("Map toggle actions to key codes")]
    private string[] toggleActions;
    [SerializeField, Tooltip("Assign key to toggle trigger")]
    private KeyCode[] toggleKey;

    // Define the tracking plane for following the mouse
    [Header("Mouse Pointer Tracking")]
    [SerializeField, Tooltip("Used to track the mouse pointer")]
    private LayerMask mouseTrackingPlane;

    // Used to set a player start point
    [HideInInspector]
    public Vector3 playerStartPoint;

    // Checks if the player is in ragdoll mode
    private bool isRagdoll = false;

    // Stores a PauseMenu
    private PauseMenu pause;

    // Unity Events to notify of player actions
    [Header("Player Events")]
    public UnityEvent playerRifleShot;
    public UnityEvent playerHandgunShot;
    public UnityEvent weaponEquipped;
    public UnityEvent respawnPlayer;
    public UnityEvent onRifleReload;
    public UnityEvent onHandgunReload;

    // Called before Start
    protected override void Awake()
    {
        // Stores the values for the player's Weapon
        playerWeapon = gameObject.GetComponent<Weapon>();

        // Calls the base (pawn class) Awake function
        base.Awake();

        // Adds an onDeath listener that triggers the player's ragdoll
        health.onDeath.AddListener(PlayerRagdoll);

        // Stores a PauseMenu instance
        pause = FindObjectOfType<PauseMenu>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        // Sets the player's start point to whereever the player first spawns in
        playerStartPoint = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // Called to aim the player at the mouse
        AimTowardMouse();

        // Called to drive player movement
        PawnMovement();

        // Called to check if the player is grounded
        CheckMiscPlayerValues();

        // Called to activate player triggers with passed in action and mapped key
        AnimationActionTriggers(playerActions, mappedKey);

        // Called to toggle triggers with passed in action and mapped key
        AnimationActionTriggerToggle(toggleActions, toggleKey);
    }

    // Function to aim the player at the mouse pointer
    void AimTowardMouse()
    {
        // Creates a ray instance called ray and sets it equal to a ray going from the main camera to the Vector3 position of the mouse pointer
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Checks if the ray, that is cast from the main camera at an infinite length, returns hit information from the defined LayerMask
        if (Physics.Raycast(ray, out var hitInfo, Mathf.Infinity, mouseTrackingPlane))
        {
            // Sets the direction variable equal to point of the ray hitInfo minus the player's transform position
            var direction = hitInfo.point - transform.position;
            // Locks the direction's 'y' value at 0
            direction.y = 0f;
            // Normalizes the direction to a value of 1
            direction.Normalize();

            if (!isRagdoll && !pause.isPaused)
            {
                // Set the player's forward transform equal to the calculated direction
                transform.forward = direction;
            }
        }
    }

    // Function to drive player movement
    void PawnMovement()
    {
        // Stores the value from the "Horizontal" input
        float horizontal = Input.GetAxis("Horizontal");
        // Stores the value from the "Vertical" input
        float vertical = Input.GetAxis("Vertical");

        // Stores a new Vector3 equal to the value of the horizontal input on the x axis, 0 on the y, and the vertical input on the z)
        Vector3 input = new Vector3(horizontal, 0f, vertical);

        // Stores the value of input with its magnitude clampped to 1
        input = Vector3.ClampMagnitude(input, 1f);
        // Multiplies the input with the player's speed
        input *= playerMovementSpeed;

        // Converts a direction in world space to local space
        // input = transform.InverseTransformDirection(input);

        // Send float values to the anim to define transitions
        anim.SetFloat("Horizontal", input.x, 0.1f, Time.deltaTime);
        anim.SetFloat("Vertical", input.z, 0.1f, Time.deltaTime);
    }

    // Function to set animation trigger boolean value
    void AnimationActionTriggers(string[] action, KeyCode[] mappedKey)
    {
        // Stores the boolean value evaluated in the ternary statement
        // If Mouse0 is held, "isShooting" is true, else false
        bool rifleShot = Input.GetKeyDown(KeyCode.Mouse0) ? true : false;
        bool handgunShot = Input.GetKeyDown(KeyCode.Mouse1) ? true : false;

        // Sets the boolean value for the anim parameter "isShooting"
        anim.SetBool("Shooting", rifleShot);
        anim.SetBool("Shooting", handgunShot);

        // Checks if the player is shooting the rifle
        if (Input.GetKeyDown(KeyCode.Mouse0) && playerWeapon != null)
        {
            if (!pause.isPaused)
            {
                if (playerWeapon.tag == "Rifle")
                {
                    // Invokes the playerRifleShot event
                    playerRifleShot.Invoke();
                }
                else
                {
                    // Invokes the playerHandgunShot event
                    playerHandgunShot.Invoke();
                }
            }
        }

        // For loop is utilized to iterate through anim parameter string array and KeyCode array
        for (int i = 0; i < playerActions.Length; i++)
        {
            // Stores the boolean value evaluated in the ternary statement
            // If the passed in key is pressed, the passed in action is true, else false
            bool trueFalse = Input.GetKeyDown(mappedKey[i]) ? true : false;

            // Sets the boolean value for the passed in anim parameter
            anim.SetBool(action[i], trueFalse);
        }
    }

    // Function to set animation triggers that are toggled
    void AnimationActionTriggerToggle(string[] action, KeyCode[] mappedKey)
    {
        // For loop to iterate through toggled actions and keys
        for (int i = 0; i < playerActions.Length; i++)
        {
            // Checks if a mapped key has been pressed
            if (Input.GetKeyDown(mappedKey[i]))
            {
                // Checks if boolean on the corresponding action is set to false
                if (anim.GetBool(action[i]) == false)
                {
                    // Sets the boolean on the action to true
                    anim.SetBool(action[i], true);
                }
                // Runs if the action trigger is set to true
                else if (anim.GetBool(action[i]) == true)
                {
                    // Sets the boolean on the action to false
                    anim.SetBool(action[i], false);
                }
            }
        }
    }

    // Function to check miscellaneous player values
    void CheckMiscPlayerValues()
    {
        // Stores the player's CapsuleCollider values
        CapsuleCollider collider = gameObject.GetComponent<CapsuleCollider>();

        if (playerWeapon != null)
        {
            weaponEquipped.Invoke();
        }

        // Checks if boolean for the prone trigger is true
        if (anim.GetBool("Prone") == true)
        {
            // Sets the height of the CapsuleCollider to 0.6
            collider.height = 0.6f;
            // Sets the center of the collider to 0.25 on the y
            collider.center = new Vector3(0f, 0.25f, 0f);
        }
        // Runs if the prone boolean is false
        else
        {
            // Sets the CapsuleCollider values to default levels
            collider.height = 1.85f;
            collider.center = new Vector3(0f, 0.91f, 0f);
        }
    }

    // Function to create a ragdoll affect for the player
    void PlayerRagdoll()
    {
        isRagdoll = true;

        // Grabs the clone's Animator and disables it
        gameObject.GetComponent<Animator>().enabled = false;

        // Calls to the base class function to set the rigidbody state to false
        SetRigidbodyState(false);

        // Calls to the base class function to set all ragdoll colliders to true
        SetColliderState(true);

        // Called to invoke the RespawnPlayer function after the respawn time
        Invoke("RespawnPlayer", health.respawnTime);
    }

    // Function to set Respawn parameters
    void RespawnPlayer()
    {
        //Set the Player's position back to the start point
        gameObject.transform.position = new Vector3(playerStartPoint.x, playerStartPoint.y, playerStartPoint.z);

        gameObject.GetComponent<Animator>().enabled = true;

        SetRigidbodyState(true);
        SetColliderState(false);

        // Sets the isRagdoll boolean back to false
        isRagdoll = false;

        respawnPlayer.Invoke();
    }

    // Function to reload after ammo pickup
    public void ReloadWeapon()
    {
        // Checks if the player is holding a rifle
        if (playerWeapon.tag == "Rifle")
        {
            // Invokes the rifle reload event
            onRifleReload.Invoke();
        }
        else // Runs if the player is holding the handgun
        {
            // Invokes the handgun reload event
            onHandgunReload.Invoke();
        }
    }

    // Function to govern inverse kinematics
    void OnAnimatorIK(int layerIndex)
    {
        // Check if the player is holding a weapon
        if (playerWeapon != null)
        {
            // Check if the weapon has a defined right and left hand position
            if (playerWeapon.playerRight && playerWeapon.playerLeft)
            {
                // Set the IK parameters for the left and right hand positions and rotations
                anim.SetIKPosition(AvatarIKGoal.LeftHand, playerWeapon.playerLeft.position);
                anim.SetIKPosition(AvatarIKGoal.RightHand, playerWeapon.playerRight.position);
                anim.SetIKRotation(AvatarIKGoal.LeftHand, playerWeapon.playerLeft.rotation);
                anim.SetIKRotation(AvatarIKGoal.RightHand, playerWeapon.playerRight.rotation);
                anim.SetIKHintPosition(AvatarIKHint.RightElbow, playerWeapon.rightElbow.position);

                // IK weight values adjust how firm the sprites hands will attach to an object
                anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
                anim.SetIKHintPositionWeight(AvatarIKHint.RightElbow, 0.6f);
            }
            else // Runs if two hand positions are not found
            {
                // Sets the IK parameters for single hand weapons
                anim.SetIKPosition(AvatarIKGoal.RightHand, playerWeapon.playerRight.position);
                anim.SetIKRotation(AvatarIKGoal.RightHand, playerWeapon.playerRight.rotation);

                anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
            }
        }
    }
}


