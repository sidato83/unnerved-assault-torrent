using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    // Store an array of enemy prefabs
    [Header("Enemy Units")]
    [SerializeField, Tooltip("An array of enemy prefabs")]
    private GameObject[] enemyPrefabs;

    // Store and array of enemy spawn points
    [Header("Spawn Points")]
    [SerializeField, Tooltip("Define where enemies will spawn")]
    private Transform[] spawnPoints;

    // Options for designers to control number and frequency of spawns
    [Header("Spawn Parameters")]
    [SerializeField, Tooltip("Define the max number of spawns on the scene")]
    private int maxNumberOfSpawns = 1;
    public int currentNumOfSpawns;
    [SerializeField, Tooltip("Define the time delay between spawns")]
    private float spawnDelay = 1f;

    // Holds an instance of the enemy Health
    private Health enemyHealth;

    // Integers used for randomization
    private int randomSpawn;
    private int randomSpawnPoint;

    // Called before Start
    private void Awake()
    {
        // Called to repeatedly spawn enemies based on the spawn delay
        InvokeRepeating("SpawnEnemy", 0f, spawnDelay);
    }

    // Function to control enemy spawning
    private void SpawnEnemy()
    {
        // Checks if the current number of spawns in the scene is more than, or equal to the max number of specified spawns
        if (currentNumOfSpawns >= maxNumberOfSpawns)
        {
            // Cancels the call to spawn function
            CancelInvoke("SpawnEnemy");
        }
        else // Runs if the spawn count is less than the specified max enemies in the scene
        {
            // Stores a random number between 0 and the total number of enemy prefabs in the array
            randomSpawn = Random.Range(0, enemyPrefabs.Length);
            // Stores a random number between 0 and the total number of spawn points in the array
            randomSpawnPoint = Random.Range(0, spawnPoints.Length);

            // Checks if the current number of spawns is less than the max number of spawns
            if (currentNumOfSpawns < maxNumberOfSpawns)
            {
                // Called to spawn random enemies from random spawn points
                RandomizedSpawning(randomSpawn, randomSpawnPoint);
            }

            // Adds a lister to the onDeath event and calls the RemoveEnemy function
            enemyHealth.onDeath.AddListener(RemoveEnemy);
        }
    }

    // Function to randomize enemy spawns and spawn points
    private void RandomizedSpawning(int randSpwn, int randSpwnPnt)
    {
        // Instantiate an enemy clone from a random enemy prefab at a random spawnpoint
        GameObject enemyClone = Instantiate(enemyPrefabs[randSpwn], spawnPoints[randSpwnPnt].position, spawnPoints[randSpwnPnt].rotation);
        enemyHealth = enemyClone.GetComponent<Health>();

        // Increase the number of spawns in the scene
        currentNumOfSpawns++;
    }

    // Function to remove an enemy after death
    private void RemoveEnemy()
    {
        // Reduce the current spawn count
        currentNumOfSpawns--;
        // Calls back to the Awake function to restart the auto-spawner
        Awake();
    }
}
