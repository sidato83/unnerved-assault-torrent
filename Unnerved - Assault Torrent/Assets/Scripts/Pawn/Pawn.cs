using UnityEngine;

// Require object to have a Health component
[RequireComponent(typeof(Health))]

public abstract class Pawn : MonoBehaviour
{
    // Health getter / setter
    protected Health health { get; private set; }
    // Animator getter / setter
    protected Animator anim { get; set; }

    // Called before start
    protected virtual void Awake()
    {
        // Called to get attached object components
        GetPawnComponents();

        // Called to set all child rigidbodies to kinematic
        SetRigidbodyState(true);
        // Called to set all child colliders to false
        SetColliderState(false);
    }

    // Function to grab pawn object components
    void GetPawnComponents()
    {
        // Sets the health to an instance of the gameObject's Health
        health = gameObject.GetComponent<Health>();
        // Sets the animator to an instance of the gameObject's Animator
        anim = gameObject.GetComponent<Animator>();
    }
    // Called to set all child rigidbodies to kinematic or affected by physics
    public void SetRigidbodyState(bool state)
    {
        // Stores a Rigidbody array and assigns all the gameObject's children Rigidbodies
        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();

        // Foreach loop that iterates through each child in the Rigidbody array
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            // Sets each rigidbody's kinematic state to the passed in value
            rigidbody.isKinematic = state;
        }
        
        // Sets the top layer Rigidbody from to the opposite state
        GetComponent<Rigidbody>().isKinematic = !state;
    }
    // Called to set all the child colliders to enabled/disabled
    public void SetColliderState(bool state)
    {
        // Stores a Collider array and assigns all the gameObject's children Colliders
        Collider[] colliders = GetComponentsInChildren<Collider>();

        // Foreach loop that iterates through each child in the collider array
        foreach (Collider collider in colliders)
        {
            // Sets the collider enabled state to the passed in value
            collider.enabled = state;
        }

        // Sets the top level Collider to the opposite state
        GetComponent<Collider>().enabled = !state;
    }
}
