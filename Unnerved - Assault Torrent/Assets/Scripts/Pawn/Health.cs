using UnityEngine.Events;
using UnityEngine;
using System;

public class Health : MonoBehaviour
{
    // Defined health values for the attached object
    [Header("Pawn Values")]
    [SerializeField, Tooltip("Pawn max health")]
    private float maxHealth = 100f;
    [HideInInspector]
    public float currentHealth = 0f;
    [Tooltip("Time to respawn after death")]
    public float respawnTime = 5f;

    // Holds a percentage of health between max and current
    [HideInInspector]
    public float healthPercentage = 0f;

    public event Action<float> OnHealthPctChanged = delegate { };
    public UnityEvent onDeath;

    // Start is called before the first frame update
    private void Awake()
    {
        // Sets the current health to the maxHealth value
        currentHealth = maxHealth;
        healthPercentage = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        // Calls the GetHealth function
        GetHealth();
    }

    // GetHealth function returns the currentHealth
    float GetHealth() => currentHealth;

    // HealthPercent function returns the value of the current health divided by the max health
    public void HealthPercent()
    {
        // Calculates and stores the health percentage
        healthPercentage = (currentHealth / maxHealth);
        // Calls the OnHealthPctChanged event and passes in the health percentage
        OnHealthPctChanged(healthPercentage);
    }

    // Function to add healing to a pawn
    public void TakeHealing(int healing)
    {
        if (currentHealth < maxHealth)
        {
            // Adds the passed in heal amount to the current health
            currentHealth += healing;
        }

        // Calls the HealthPercet function
        HealthPercent();
    }

    // Function to add damage to the pawn
    public void TakeDamage(int damage)
    {
        // Checks if the current health is less than 1
        if (currentHealth < 1)
        {
            // Calls the DeadPawn function
            DeadPawn();
        }
        // Runs if the current health is greater than or equal to 1
        else
        {
            // Subtracts the passed in damage from the current health
            currentHealth -= damage;

            // Calls the HealthPercet function
            HealthPercent();
        }
    }

    // Function to define what happens to a dead pawn
    public void DeadPawn()
    {
        // Invokes the onDeath event
        onDeath.Invoke();

        // Checks if the attached parent is of type Player
        if (gameObject.GetComponentInParent<Player>())
        {
            // Set all health values to 0
            currentHealth = 0f;
            healthPercentage = 0f;

            HealthPercent();
        }
        else
        {
            // Remove enemy collider during ragdoll death
            Collider pawnCollider = gameObject.GetComponentInParent<Collider>();
            pawnCollider.enabled = false;

            // If the pawn is of type Enemy, it will be destroyed
            // Delay added to show ragdoll death
            Destroy(gameObject, 4);
        }
    }

    // Function to respawn the player
    public void RespawnPlayer()
    {
        // Reset health values
        currentHealth = maxHealth;
        healthPercentage = 1f;

        // Called to display the reset health values
        HealthPercent();
    }
}
