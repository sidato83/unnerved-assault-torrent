using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Enemy : MonoBehaviour
{
    [Header("Enemy Navigation")]
    [SerializeField, Tooltip("Set an enemy target")]
    private Transform playerTarget;
    private Vector3 desiredVelocity;

    private NavMeshAgent enemyNavMesh;
    private Animator anim;

    // Awake is called before start
    private void Awake()
    {
        enemyNavMesh = gameObject.GetComponent<NavMeshAgent>();
        anim = gameObject.GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMovement();
        ChaseTarget(playerTarget);
    }

    private void OnAnimatorMove()
    {
        enemyNavMesh.velocity = anim.velocity;
    }

    void EnemyMovement()
    {
        desiredVelocity = Vector3.MoveTowards(desiredVelocity, enemyNavMesh.desiredVelocity, enemyNavMesh.acceleration * Time.deltaTime);
       
        Vector3 input = transform.InverseTransformDirection(desiredVelocity);

        anim.SetFloat("Horizontal", input.x);
        anim.SetFloat("Vertical", input.z);
    }

    void ChaseTarget(Transform target)
    {
        enemyNavMesh.SetDestination(target.position);
    }
}
