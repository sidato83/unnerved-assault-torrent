using UnityEngine;
using UnityEngine.Events;

public abstract class Weapon : MonoBehaviour
{
    // Stores the transforms for the inverse kinematic values
    [Header("IK Points")]
    [Tooltip("Weapon right hand placement")]
    public Transform rightHandPosition;
    [Tooltip("Weapon left hand placement")]
    public Transform leftHandPosition;

    // Creates events in the inspector
    [Header("Firing Events")]
    public UnityEvent OnAttackStart;
    public UnityEvent OnAttackEnd;

    // Update is called once per frame
    private void Update()
    {
        // Calls the AttackStart function
        AttackStart();
    }

    // Function to start a weapon attack
    public virtual void AttackStart()
    {
        // Invokes the OnAttackStart event
        OnAttackStart.Invoke();
    }

    // Function to define the sequence for the end of an attack
    public virtual void AttackEnd()
    {
        // Invokes the OnAttackEnd event
        OnAttackEnd.Invoke();
    }
}
