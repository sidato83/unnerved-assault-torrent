using UnityEngine;

// Handgun subclass inherits from Weapon
public class WeaponHandgun : Weapon
{
    // Handgun values
    [Header("Handgun Values")]
    [Tooltip("Handgun bullet capacity")]
    public int bulletCount = 18;

    // Bullet prefab and fire position
    [Header("Handgun Components")]
    [SerializeField, Tooltip("Define the bullet prefab")]
    private GameObject bulletPrefab;
    [SerializeField, Tooltip("Point to instantiate the bullet")]
    private Transform bulletFirePosition;

    // Override for the base AttackStart
    public override void AttackStart()
    {
        // Calls the function to fire the gun
        FireGun();
    }

    // Override for the base AttackEnd (currently not defined)
    public override void AttackEnd()
    {
        // Calls the base AttackEnd function
        base.AttackEnd();
    }

    // Function to fire the handgun
    private void FireGun()
    {
        // Checks if the gun has bullets
        if (bulletCount > 0)
        {
            // Checks if the bullet prefab exists and the right mouse key is down
            if (bulletPrefab != null && Input.GetKeyDown(KeyCode.Mouse1))
            {
                // Instantiates the bulletPrefab at the fire postion and rotation
                GameObject gunBullet = Instantiate(bulletPrefab, bulletFirePosition.position, bulletFirePosition.rotation);
                bulletCount--;
            }
        }
        // Runs if the gun is out of bullets
        else if (bulletCount <= 0)
        {
            // Display message to reload (just in the console for now)
            Debug.Log("RELOAD GUN");
        }
    }
}